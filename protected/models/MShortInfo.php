<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dmaslov
 * Date: 27.09.12
 * Time: 16:20
 * To change this template use File | Settings | File Templates.
 */
class MShortInfo extends MDoc
{

    public static function getMainTaxon() {
        return "info";
    }
    public function getXAttrsDef() {
        return array_merge(parent::getXAttrsDef(),array(
            array('name'=>'color','type'=>'string'),
            array('name'=>'priority','type'=>'string'),
            array('name'=>'weight','type'=>'int')

        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function markDelete() {
     $this->isdelete=1;
     return $this->save();
    }

    public function defaultScope() {
        return array(
           'condition'=>"taxon LIKE '".static::getMainTaxon()."%'"
        );
    }
    public function rules()
    {
        $rules=parent::rules();
        $rules[]=array('opdate, details, color, weight, priority', 'safe');
        return $rules;
    }
}

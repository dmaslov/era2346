<?php
/**
 * Created by JetBrains PhpStorm.
 * User: user
 * Date: 11/27/12
 * Time: 10:23 PM
 * To change this template use File | Settings | File Templates.
 */
interface IXattrable
{
    public function getTableName();
    public function getPK();

    /**
     * Method returns definition of xattrs.
     * @return array
     */
    public function getXAttrsDef();

    /**
     * Method returns TRUE if XAttr $key is exists.
     * @param $key
     * @return mixed
     */
    public function isXAttrDef($key);

    /**
     * Method returns xattr value.
     * @param $key
     * @param null $defValue
     * @return mixed
     */
    public function getXAttr($key,$defValue=null);

    /**
     * Method returns array with all xattrs for this object.
     * @return mixed
     */
    public function getAllXAttrs();

    /**
     * Method saves xattr values to DB.
     * @return mixed
     */
    public function saveAllXAttrs();
    public function setXAttr($key,$value,$date = NULL);
    public function delXAttr($key);

}

<?php
/**
 * Created by JetBrains PhpStorm.
 * User: user
 * Date: 11/27/12
 * Time: 10:24 PM
 * To change this template use File | Settings | File Templates.
 */
abstract class MAr extends CActiveRecord  implements IXattrable
{
    private $_xattrs=array();

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function getTableName()
    {
        return $this->getTableSchema()->name;
    }

    public function getPK() {
        return $this->primaryKey;
    }

    public function getXAttrsDef()
    {
        return array(
            array('name'=>'test1','type'=>'string')
        );
    }

    public function isXAttrDef($key)
    {

        foreach ($this->getXAttrsDef() as $xAttrDef) {

            if ($xAttrDef['name']==$key) {
                     return $xAttrDef;
            }
        }
        return FALSE;
    }


    public function getAllXAttrs()
    {
        $command=Yii::app()->db->createCommand();
        $xattrs=$command->select('*')
            ->from('xattr')
            ->where('stbl=:stbl AND sid=:sid',
            array(':stbl'=>$this->getTableName(),':sid'=>$this->getPK())
        )
            ->queryAll();
        return $xattrs;
    }

    public function getXAttr($key, $defValue = NULL)
    {

        if ($this->isXAttrDef($key)) {
            if (isset($this->_xattrs[$key])) {
                return $this->_xattrs[$key];
            } else {
                return $defValue;
            };
        };
        throw new CException("Property is't exists");
    }

    public function setXAttr($key,$value,$date = NULL) {
        if ($this->isXAttrDef($key)) {
            $this->_xattrs[$key]=$value;
        } else {
            throw new CException("Property is't exists");
        }
    }

    public function delXAttr($key) {
        if ($this->isXAttrDef($key)) {
            $this->_xattrs[$key]=NULL;
        } else {
            throw new CException("Property is't exists");
        };
    }
    public function saveAllXAttrs() {


        foreach ($this->_xattrs as $key=>$v) {

            $res=Yii::app()->db->createCommand()
                         ->select('value')
                         ->from('xattr')
                         ->where('stbl=:stbl AND sid=:sid AND code=:code',array(':stbl'=>$this->getTableName(),':sid'=>$this->getPK(),':code'=>$key))
                         ->queryRow();

            if ($res) {
                if ($res['value']!=$v) {
                      $res=Yii::app()->db->createCommand()->update('xattr',array('value'=>$v),'stbl=:stbl AND sid=:sid AND code=:key',array(':stbl'=>$this->getTableName(),':sid'=>$this->getPK(),':key'=>$key));
                } else {
                    $res=true;
                };
            } else {
                $res=Yii::app()->db->createCommand()->insert('xattr',array('stbl'=>$this->getTableName(),'sid'=>$this->getPK(),'code'=>$key,'value'=>$v));
            };
/*
            if (!$res) {
                return false;
            };
*/
        };
        return true;
    }
    protected function afterFind() {
        $xattrs=$this->getAllXAttrs();

        foreach ($xattrs as $xattr) {
            $xattrDef=$this->isXAttrDef($xattr['code']);

            switch ($xattrDef['type']) {
                case 'int':
                    $this->_xattrs[$xattr['code']]=(int) $xattr['value'];
                    break;
                case 'boolean':
                    $this->_xattrs[$xattr['code']]=(bool) $xattr['value'];
                    break;
                default:
                    $this->_xattrs[$xattr['code']]=$xattr['value'];
                    break;
            }

        };

        return parent::afterFind();
    }

    public  function __get($name) {
        if ($this->isXAttrDef($name)) {
            return $this->getXAttr($name);
        }
        else {
            return parent::__get($name);
        }
    }
    public  function __set($name,$value) {
        if ($this->isXAttrDef($name)) {
            $this->setXAttr($name,$value);
        } else {
            parent::__set($name,$value);
        }
    }
    public function save($runValidation=true,$attributes=NULL) {
        //$tr=MUser::model()->dbConnection->beginTransaction();
        //!!! Важно сперва сохраняем модель
        // затем все остальное!!!
        if (parent::save($runValidation,$attributes)) {

            if ($this->saveAllXAttrs()) {
                return true;
          //      $tr->commit();
            } else {
            //    $tr->rollBack();
                return false;
            };
        } else {
            //$tr->rollBack();
        };
        //$tr->rollBack();*/
        return false;
    }
}

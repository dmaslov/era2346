<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dmaslov
 * Date: 23.01.13
 * Time: 16:45
 * To change this template use File | Settings | File Templates.
 */
class MDocTest extends CDbTestCase
{

    public $fixtures=array(
        'docs'=>'Doc',
        'opdate'=>'Opdate',
        'status'=>'Status',
        'xattr'=>':xattr'
    );

    public function testAdd() {

        $opdate=new MOpdate();
        $opdate->opdate='2013-01-01';
        $this->assertTrue($opdate->save());

        $status=new MStatus();
        $status->name='Введен';
        $this->assertTrue($status->save());

        $user = new MShortInfo();
        $user->setAttributes(array(
           'opdate'=>'2013-01-01',
           'num'=>'111',
           'details'=>'sdfsfsdfsdf'
        ));
        $this->assertTrue($user->save());

        $newShortInfoId=$user->primaryKey;

        $user2 = new MShortInfo();
        $user->setAttributes(array(
            'opdate'=>'2013-01-02',
            'num'=>'111',
            'details'=>'sdfsfsdfsdf'
        ));
        $this->assertFalse($user->save());

        $user3 = MShortInfo::model()->findByPk($newShortInfoId);

        $this->assertTrue($user3 instanceof MShortInfo);

        $user4 = MShortInfo::model()->findByAttributes(array('num'=>'111'));

        $this->assertTrue($user4 instanceof MShortInfo);

        Yii::app()->onEndRequest(new CEvent(null));
    }

    public function testXAttrs() {
        $opdate=new MOpdate();
        $opdate->opdate='2013-01-01';
        $this->assertTrue($opdate->save());

        $status=new MStatus();
        $status->name='Введен';
        $this->assertTrue($status->save());


        $doc=new MShortInfo();
        $doc->setAttributes(array(
            'opdate'=>'2013-01-01',
            'num'=>'2',
            'details'=>'Тестовый пример 2',
            'color'=>'red'
        ));
        $doc->weight='15';
        $doc->priority="hight";
        $this->assertTrue($doc->save());
        $newDocId=$doc->primaryKey;

        $doc2=MShortInfo::model()->findByPk($newDocId);
        $this->assertEquals($doc2->color,'red');
        $this->assertEquals($doc2->weight,'15');
        $this->assertEquals($doc2->priority,'hight');


    }
    /**
     * @expectedException Exception
     * @expectedExceptionMessage Unknown property 'mySuperPuperXAttr' for class 'MDocTest'
     */
    public function testXAttrs2() {
        $opdate=new MOpdate();
        $opdate->opdate='2013-01-01';
        $this->assertTrue($opdate->save());

        $status=new MStatus();
        $status->name='Введен';
        $this->assertTrue($status->save());


        $doc=new MShortInfo();
        $doc->setAttributes(array(
            'opdate'=>'2013-01-01',
            'num'=>'2',
            'details'=>'Тестовый пример 2',
            'color'=>'red'
        ));
        $this->assertTrue($doc->save());
        echo $this->mySuperPuperXAttr;
    }
}
